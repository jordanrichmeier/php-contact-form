#### Dealer Inspire PHP Code Challenge ####

Welcome to my completed Dealer Inspire PHP code challenge.

#### Prerequisites ####

* [Docker](https://docs.docker.com/engine/installation/)
* [Docker-compose](https://docs.docker.com/compose/install/)

#### Getting Started ####

I've opted to utilize docker in my challenge to facilitate smooth functioning code
without having to worry about php or mysql version compatibilities.

I've included three handy bash scripts with which you can run to easily facilitate
the review of this coding challenge. Before running them please make sure that you've
stopped any local running apache2 or mysql instances as docker will need access
to these ports.

Here are the commands you will need to run after cloning the repository in order
to get up and running.

```
cd this-repo-checkout
./run-docker
./mysql-import
./phpunit
```

With docker up the challenge will be accessable through localhost or 127.0.0.1
in a browser.

#### Emails ####

The docker configuration uses mailcatcher for testing email in development. To
view sent emails, open http://localhost:1080.

#### Composer ####

I've made the decision to include all composer libraries with the repo. This decision
was simply to help get the challenge up and running with fewer steps.
Normally vendor directories are to be ignored and be managed completely by composer.

#### Project Guidlines ####

Listing project requirements for perpetuity.

* Create a functioning contact form with the following inputs: Full Name, Email, Message, Phone
* Contact form should match style of the rest of the page
* Provide validation for inputs
* All inputs are to be required except for the phone
* On submit an email should be sent to guy-smiley@example.com containing the message
* On submit a copy of the message is to be saved into the database for later consumption
* PHPUnit tests should be provided for all php funtionality

#### Additions ####

Before I began this project I decided to expand on the requirements with a few
improvements that I felt were necessary for a more professional feel.

* reCaptcha should be included to prevent spam
* An email template should be used for both style and future proofing
* All inputs should be sanitized to prevent script injection
* Inputs should be protected against any sql injection attempts

#### Conclusion ####

Thank you for allowing me to showcase my abilities and I hope you enjoy
reviewing my code.

I look forward to going over it with you and hearing your input.