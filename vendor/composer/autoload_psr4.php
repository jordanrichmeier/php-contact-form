<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'phpDocumentor\\Reflection\\' => array($vendorDir . '/phpdocumentor/reflection-common/src', $vendorDir . '/phpdocumentor/type-resolver/src', $vendorDir . '/phpdocumentor/reflection-docblock/src'),
    'Webmozart\\Assert\\' => array($vendorDir . '/webmozart/assert/src'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Component\\EventDispatcher\\' => array($vendorDir . '/symfony/event-dispatcher'),
    'Slim\\Views\\' => array($vendorDir . '/slim/php-view/src'),
    'Slim\\PDO\\' => array($vendorDir . '/slim/pdo/src/PDO'),
    'Slim\\Flash\\' => array($vendorDir . '/slim/flash/src'),
    'Slim\\' => array($vendorDir . '/slim/slim/Slim'),
    'Respect\\Validation\\' => array($vendorDir . '/respect/validation/library'),
    'ReCaptcha\\' => array($vendorDir . '/google/recaptcha/src/ReCaptcha'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
    'Psr\\Container\\' => array($vendorDir . '/psr/container/src'),
    'Interop\\Container\\' => array($vendorDir . '/container-interop/container-interop/src/Interop/Container'),
    'FastRoute\\' => array($vendorDir . '/nikic/fast-route/src'),
    'Egulias\\EmailValidator\\' => array($vendorDir . '/egulias/email-validator/EmailValidator'),
    'Doctrine\\Instantiator\\' => array($vendorDir . '/doctrine/instantiator/src/Doctrine/Instantiator'),
    'DeepCopy\\' => array($vendorDir . '/myclabs/deep-copy/src/DeepCopy'),
    'DealerInspire\\Model\\' => array($baseDir . '/public/models'),
    'DealerInspire\\Library\\' => array($baseDir . '/public/libraries'),
    'DealerInspire\\Controller\\' => array($baseDir . '/public/controllers'),
    'DealerInspire\\' => array($baseDir . '/public'),
);
