<?php
$ENV = getenv('APP_ENV') ?? 'PROD';
$config = [
    'settings' => [
        'displayErrorDetails' => $ENV != 'PROD',
        'mailer' => [
            'host'     => getenv('SMTP_HOST') ?: '127.0.0.1',
            'username' => getenv('SMTP_USER') ?: 'username',
            'password' => getenv('SMTP_PASS') ?: 'password',
            'port'     => getenv('SMTP_PORT') ?: '1025'
        ],
        'db' => [
            'driver'        => 'mysql',
            'host'          => getenv('MYSQL_ADDRESS') ?: 'localhost',
            'database'      => getenv('MYSQL_DATABASE') ?: 'challenge',
            'port'          => getenv('MYSQL_PORT') ?: '3306',
            'user'          => getenv('MYSQL_USERNAME') ?: 'dealerinspire',
            'pass'          => getenv('MYSQL_PASSWORD') ?: 'challenge'
        ],
        'recaptchaSecretKey' => '6LcVq0YUAAAAAOt9dtj57B9kBWpnmV0HlnebmQtJ'
    ]
];