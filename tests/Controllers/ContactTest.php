<?php
require 'vendor/autoload.php';
require_once 'tests/Helpers/MailCatcherUtil.php';

use PHPUnit\Framework\TestCase;
use DealerInspire\App;
use Slim\Http\Environment;
use Slim\Http\Request;

CONST APP_DIR = __dir__ . "/../../public";
CONST BY_PASS_CAPTCHA = true;

/**
 * ContactFormTest Class
 *
 * @author		Jordan Richmeier
 * @email 		jordan@dealerinspire.com
 * @extends		TestCase
 *
 * Unit tests for the contact forms functionality
 *
 */
class ContactTest extends TestCase
{
    private $app;
    private $mailcatcher;
    private $messagesModel;

    public function setUp() {
        include(APP_DIR . '/../config.php');
        $config['settings']['db']['database'] = 'challenge_TEST';
        $this->app = (new App($config))->get();
        $this->mailcatcher = new MailCatcherUtil();
        $this->mailcatcher->cleanMessages();
        $db = $this->app->getContainer()->get('database');
        $this->messagesModel = new \DealerInspire\Model\ContactMessages($db);
        $this->messagesModel->clear();
    }

    public function testFormPresence() {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/',
        ]);
        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);

        $html = (string)$response->getBody();

        $this->assertSame($response->getStatusCode(), 200);
        $this->assertContains('name="fname"', $html);
        $this->assertContains('name="lname"', $html);
        $this->assertContains('name="email"', $html);
        $this->assertContains('name="message"', $html);
        $this->assertContains('name="phone"', $html);
        $this->assertContains('class="g-recaptcha"', $html);
    }

    public function testSubmitSuccessFlashMessage() {
        $this->setUpPostEnv();
        $response = $this->app->run(true);

        $flash = new \Slim\Flash\Messages();
        $messages = $flash->getMessages();

        $this->assertSame($response->getStatusCode(), 202);
        $this->assertTrue(isset($messages['success']));
    }

    public function testSubmitPhoneOptional() {
        $postData = $this->getGenericPostData();
        unset($postData['phone']);
        $this->setUpPostEnv($postData);
        $response = $this->app->run(true);

        $flash = new \Slim\Flash\Messages();
        $messages = $flash->getMessages();

        $this->assertSame($response->getStatusCode(), 202);
        $this->assertTrue(isset($messages['success']));
    }

    public function testSubmitSendsEmail() {
        $postData = $this->getGenericPostData();
        $this->setUpPostEnv($postData);
        $response = $this->app->run(true);

        $email = $this->mailcatcher->getLastMessage();
        $emailBody = $this->mailcatcher->getMessagePlain($email->id);

        $this->assertSame($response->getStatusCode(), 202);
        $this->assertSame($email->sender, "<{$postData['email']}>");
        $this->assertSame($email->recipients[0], '<guy-smiley@example.com>');
        $this->assertSame($email->subject, 'Contact Request');
        $this->assertContains($postData['message'], $emailBody);
    }

    public function testSubmitSavesToDatabase() {
        $postData = $this->getGenericPostData();
        $this->setUpPostEnv($postData);
        $response = $this->app->run(true);

        $dbData = $this->messagesModel->getMostRecent();

        $this->assertSame($response->getStatusCode(), 202);
        $this->assertSame($dbData['first_name'], $postData['fname']);
        $this->assertSame($dbData['last_name'], $postData['lname']);
        $this->assertSame($dbData['email'], $postData['email']);
        $this->assertSame($dbData['phone'], $postData['phone']);
        $this->assertSame($dbData['message'], $postData['message']);
    }

    public function testSubmitBadData() {
        $postData = [
            'fname'   => '',
            'lname'   => "Danger",
            'email'   => 'malformed',
            'phone'   => '326-437-24',
            'message' => ''
        ];
        $this->setUpPostEnv($postData);
        $response = $this->app->run(true);

        $flash = new \Slim\Flash\Messages();
        $messages = $flash->getMessages();

        $this->assertSame($response->getStatusCode(), 302);
        $this->assertTrue(isset($messages['danger']));
        $this->assertContains("required", $messages['danger'][0]);
        $this->assertContains("required", $messages['danger'][1]);
        $this->assertContains("email address", $messages['danger'][2]);
        $this->assertContains("XXX-XXX-XXXX", $messages['danger'][3]);
    }

    public function testSubmitSqlInjection() {
        $postData = [
            'fname'   => "x')DROP TABLE contact_messages;",
            'lname'   => "i')DROP TABLE contact_messages;",
            'email'   => "dangerDennis11@gmail.com",
            'phone'   => "326-437-9324",
            'message' => "Message.')DROP TABLE contact_messages;"
        ];
        $this->setUpPostEnv($postData);
        $response = $this->app->run(true);

        $dbData = $this->messagesModel->getAll();

        $this->assertSame($response->getStatusCode(), 202);
        $this->assertCount(1, $dbData);
        $this->assertSame(
            $dbData[0]['first_name'],
            "x')DROP TABLE contact_messages;"
        );
        $this->assertSame(
            $dbData[0]['last_name'],
            "i')DROP TABLE contact_messages;"
        );
        $this->assertSame(
            $dbData[0]['message'],
            "Message.')DROP TABLE contact_messages;"
        );
    }

    private function setUpPostEnv($postData = null) {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI'    => '/submit',
            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded'
        ]);
        $postData = $postData ?? $this->getGenericPostData();
        $req = Request::createFromEnvironment($env)->withParsedBody($postData);
        $this->app->getContainer()['request'] = $req;
    }

    private function getGenericPostData() {
        $postData = [
            'fname'   => 'Dennis',
            'lname'   => 'Danger',
            'email'   => 'dangerDennis11@gmail.com',
            'phone'   => '326-437-9324',
            'message' => 'Fun Fact: Danger is also my middle name.'
        ];
        return $postData;
    }
}
