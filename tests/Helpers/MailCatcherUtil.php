<?php

/**
 * MailCatcherUtil Class
 *
 * @author		Jordan Richmeier
 * @email       jordan@dealerinspire.com
 *
 * Uses Guzzle Http Client to access mailcatcher through
 * the mailcatcher API. Used as a test utility to confirm
 * the contents and succesful sending of email.
 *
 */
class MailCatcherUtil
{
    private $catcher;

    public function __construct() {
        $this->mailcatcher = new \Guzzle\Http\Client('http://email:1080/');
    }

    public function cleanMessages()
    {
        $this->mailcatcher->delete('/messages')->send();
    }

    public function getLastMessage()
    {
        $messages = $this->getMessages();
        // messages are in descending order
        return reset($messages);
    }

    public function getMessages()
    {
        $jsonResponse = $this->mailcatcher->get('/messages')->send();
        return json_decode($jsonResponse->getBody());
    }
    
    public function getMessageHtml($id) {
        $response = $this->mailcatcher->get("/messages/{$id}.html")->send();
        return (string)$response->getBody();
    }

    public function getMessagePlain($id) {
        $response = $this->mailcatcher->get("/messages/{$id}.plain")->send();
        return (string)$response->getBody();
    }
}
