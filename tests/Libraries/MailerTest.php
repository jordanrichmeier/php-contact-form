<?php
require 'vendor/autoload.php';
require_once 'tests/Helpers/MailCatcherUtil.php';

use PHPUnit\Framework\TestCase;
CONST APP_DIR = __dir__ . "/../../public";

/**
 * MailerTest Class
 *
 * @author		Jordan Richmeier
 * @email 		jordan@dealerinspire.com
 * @extends		TestCase
 *
 * Tests the functionality of the mailer library
 *
 */
class MailerTest extends TestCase
{
    private $app;
    private $mailer;
    private $mailcatcher;

    public function setUp() {
        include(APP_DIR . '/../config.php');
        $this->mailer = new \DealerInspire\Library\Mailer(
            $config['settings']['mailer']
        );
        $this->mailcatcher = new MailCatcherUtil();
        $this->mailcatcher->cleanMessages();
    }

    public function testSendRawMessage() {
        $subject = 'Crime Fighters Anonymous';
        $from = 'mintBerryCrunch@coonandfriends.com';
        $to = 'mysterion@coonrivals.com';
        $message = 'Shablagoo';
        $this->mailer->send($subject, $from, $to, $message);

        $email = $this->mailcatcher->getLastMessage();
        $emailBody = $this->mailcatcher->getMessagePlain($email->id);

        $this->assertSame($email->sender, "<$from>");
        $this->assertSame($email->recipients[0], "<$to>");
        $this->assertSame($email->subject, $subject);
        $this->assertContains($message, $emailBody);
    }

    public function testSendTemplate() {
        $templateData = [
            'fname' => 'Mint',
            'lname' => 'Crunch',
            'email' => 'mintBerryCrunch@coonandfriends.com',
            'phone' => '624-427-6937',
            'message' => 'Shablagoo'
        ];
        $subject = 'Crime Fighters Anonymous';
        $from = 'mintBerryCrunch@coonandfriends.com';
        $to = 'mysterion@coonrivals.com';
        $this->mailer->setTemplate('contactForm.php', $templateData)->send(
            $subject,
            $from,
            $to
        );

        $email = $this->mailcatcher->getLastMessage();
        $emailBody = $this->mailcatcher->getMessagePlain($email->id);

        $this->assertSame($email->sender, "<$from>");
        $this->assertSame($email->recipients[0], "<$to>");
        $this->assertSame($email->subject, $subject);
        $this->assertContains($templateData['fname'], $emailBody);
        $this->assertContains($templateData['lname'], $emailBody);
        $this->assertContains($templateData['email'], $emailBody);
        $this->assertContains($templateData['phone'], $emailBody);
        $this->assertContains($templateData['message'], $emailBody);
    }
}
