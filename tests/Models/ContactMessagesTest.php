<?php
require 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use DealerInspire\App;
use Slim\Http\Environment;
use Slim\Http\Request;

CONST APP_DIR = __dir__ . "/../../public";

/**
 * ContactMessagesTest Class
 *
 * @author		Jordan Richmeier
 * @email 		jordan@dealerinspire.com
 * @extends		TestCase
 *
 * Test CRUD operations of the ContactMessages Model
 *
 */
class ContactMessagesTest extends TestCase
{
    private $app;
    private $messagesModel;

    public function setUp() {
        include(APP_DIR . '/../config.php');
        $config['settings']['db']['database'] = 'challenge_TEST';
        $this->app = (new App($config))->get();
        $db = $this->app->getContainer()->get('database');
        $this->messagesModel = new \DealerInspire\Model\ContactMessages($db);
        $this->messagesModel->clear();
    }

    public function testInsertAndGet() {
        $insertData = $this->getGenericInsertData();
        $id = $this->messagesModel->insert($insertData);

        $data = $this->messagesModel->get($id);

        $this->assertSame($data['first_name'], 'The');
        $this->assertSame($data['last_name'], 'Rock');
        $this->assertSame($data['email'], 'smellwhat@therockiscooking.com');
        $this->assertSame($data['phone'], '426-243-1625');
        $this->assertSame(
            $data['message'],
            "I'm not sure why I chose the rock for testing..."
        );
    }

    public function testGetAll() {
        $insertData = $this->getGenericInsertData();
        $this->messagesModel->insert($insertData);
        $this->messagesModel->insert($insertData);

        $data = $this->messagesModel->getAll();

        $this->assertCount(2, $data);
    }

    public function testGetByEmailAddress() {
        $insertData = $this->getGenericInsertData();
        $this->messagesModel->insert($insertData);
        $this->messagesModel->insert($insertData);

        $data = $this->messagesModel->getByEmailAddress(
            'smellwhat@therockiscooking.com'
        );

        $this->assertCount(2, $data);
        $this->assertSame($data[0]['first_name'], 'The');
        $this->assertSame($data[0]['last_name'], 'Rock');
        $this->assertSame($data[0]['email'], 'smellwhat@therockiscooking.com');
        $this->assertSame($data[0]['phone'], '426-243-1625');
        $this->assertSame(
            $data[0]['message'],
            "I'm not sure why I chose the rock for testing..."
        );
    }

    public function testGetMostRecent() {
        $insertData = $this->getGenericInsertData();
        $this->messagesModel->insert($insertData);
        $insertData[4] = "This is the most recent message";
        $this->messagesModel->insert($insertData);

        $data = $this->messagesModel->getMostRecent();

        $this->assertSame($data['message'],"This is the most recent message");
    }

    public function testUpdate() {
        $insertData = $this->getGenericInsertData();
        $id = $this->messagesModel->insert($insertData);
        $updateData = [
            'message' => 'and lo thine message hath been update...ith'
        ];

        $this->messagesModel->update($id, $updateData);
        $data = $this->messagesModel->get($id);

        $this->assertSame(
            $data['message'],
            "and lo thine message hath been update...ith"
        );
    }

    public function testDelete() {
        $insertData = $this->getGenericInsertData();
        $id = $this->messagesModel->insert($insertData);

        $this->messagesModel->delete($id);
        $data = $this->messagesModel->get($id);

        $this->assertFalse($data);
    }

    public function testClear() {
        $insertData = $this->getGenericInsertData();
        $this->messagesModel->insert($insertData);
        $this->messagesModel->insert($insertData);
        $this->messagesModel->insert($insertData);

        $this->messagesModel->clear();
        $data = $this->messagesModel->getAll();

        $this->assertCount(0, $data);
    }

    private function getGenericInsertData() {
        $data = [
            'The',
            'Rock',
            'smellwhat@therockiscooking.com',
            '426-243-1625',
            "I'm not sure why I chose the rock for testing..."
        ];
        return $data;
    }
}
