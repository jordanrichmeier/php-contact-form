<?php
namespace DealerInspire;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
 * App Class
 *
 * @author		Jordan Richmeier
 * @email 		jordan@dealerinspire.com
 *
 * Handles the intial configuration of the slim app
 *
 */
class App
{
    protected $app;
    private $config;
    
    public function __construct($config) {
        if (session_status() == PHP_SESSION_NONE) {
            @session_start();
        }
        $container = new \Slim\Container($config);
        $container = $this->setupDatabase($container);
        $app = new \Slim\App($container);
        $this->app = (new Routes($app))->getRoutedApp();
    }

    public function get()
    {
        return $this->app;
    }

    private function setupDatabase($container) {
        $newContainer = $container;
        $config = $newContainer->get('settings')['db'];
        $dsn = 'mysql:host=' . $config['host']
                . ';port=' . $config['port']
                . ';dbname=' . $config['database']
                . ';charset=utf8';
        $newContainer['database'] = new \Slim\PDO\Database(
            $dsn,
            $config['user'],
            $config['pass']
        );

        return $newContainer;
    }
}