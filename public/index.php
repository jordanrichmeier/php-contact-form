<?php
CONST APP_DIR = __dir__;

require '../vendor/autoload.php';
include_once(APP_DIR . '/../config.php');

$app = (new DealerInspire\App($config))->get();
$app->run();