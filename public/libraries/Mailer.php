<?php
namespace DealerInspire\Library;

/**
 * Mailer Class
 *
 * @author  Jordan Richmeier
 * @email   jordan@dealerinspire.com
 *
 * SwiftMailer abstraction layer
 *
 */
class Mailer
{
    private $swiftMailer;
    private $template = null;
    
    public function __construct($config) {
        $transport = (new \Swift_SmtpTransport($config['host'], 25))
          ->setUsername($config['username'])
          ->setPassword($config['password'])
          ->setPort($config['port']);
        $this->swiftMailer = new \Swift_Mailer($transport);
    }

    public function setTemplate($name, $data) {
        $view = new \Slim\Views\PhpRenderer(APP_DIR . "/views/mailers");
        $newResponse = $view->render(new \Slim\Http\Response(), $name, $data);
        $newResponse->getBody()->rewind();
        $this->template = $newResponse->getBody()->getContents();
        return $this;
    }

    public function send($subject, $from, $recipients, $message = null) {
        if ($this->template) {
            $message = $this->template;
        }
        $message = (new \Swift_Message($subject))
          ->setFrom($from)
          ->setTo($recipients)
          ->setBody($message);
        return $this->swiftMailer->send($message);
    }
}