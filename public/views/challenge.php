<?php include($viewPart['header']); ?>
    <!-- Intro Header -->
    <header class="intro">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="brand-heading">Challenge</h1>
                        <p class="intro-text">Code Something Awesome.
                            <br>We &lt;3 PHP Developers.</p>
                        <a href="#about" class="btn btn-circle page-scroll">
                            <i class="fa fa-angle-double-down animated"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- About Section -->
    <section id="about" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>About This Challenge</h2>
                <p>We make awesome things at Dealer Inspire.  We'd like you to join us.  That's why we made this page.  Are you ready to join the team?</p>
                <p>To take the code challenge, visit <a href="https://bitbucket.org/dealerinspire/php-contact-form">this Git Repo</a> to clone it and start your work.</p>
            </div>
        </div>
    </section>

    <section id="coffee" class="content-section text-center">
        <div class="download-section">
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2">
                    <h2>Coffee Break?</h2>
                    <p>Take a coffee break.  You deserve it.</p>
                    <a href="https://www.youtube.com/dealerinspire" target="_blank" class="btn btn-default btn-lg">or Watch YouTube</a>
                </div>
            </div>
        </div>
    </section>

    <!-- Contact Section -->
    <section id="contact" class="container content-section text-center">
        <?php if ($messages) { ?>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <?php foreach ($messages as $type => $msgs) { ?>
                        <div class="alert alert-<?=$type?>">
                            <?=implode("<br />", $msgs)?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Contact Guy Smiley</h2>
                <p>Remember Guy Smiley?  Yeah, he wants to hear from you.</p>
                <p class="bg-primary">
                    <form id="contactForm" action="submit" method="POST">
                        <div class="row">
                            <div class="form-group col-lg-6">
                                <label for="fname">
                                    First Name
                                    <small class="text-danger">*</small>
                                </label>
                                <input
                                    class="form-control"
                                    type="text"
                                    name="fname"
                                    id="fname"
                                    placeholder="Enter Your First Name"
                                    value="<?=$formData['fname'] ?? ""?>"
                                    maxlength="35"
                                    required>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="lname">
                                    Last Name
                                    <small class="text-danger">*</small>
                                </label>
                                <input
                                    class="form-control"
                                    type="text"
                                    name="lname"
                                    id="lname"
                                    placeholder="Enter Your Last Name"
                                    value="<?=$formData['lname'] ?? ""?>"
                                    maxlength="35"
                                    required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6">
                                <label for="email">
                                    Email Address
                                    <small class="text-danger">*</small>
                                </label>
                                <input
                                    class="form-control"
                                    type="email"
                                    name="email"
                                    id="email"
                                    placeholder="Enter Your Email Address"
                                    aria-describedby="emailHelp"
                                    value="<?=$formData['email'] ?? ""?>"
                                    maxlength="255"
                                    required>
                                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="phone">Phone</label>
                                <input
                                    class="form-control input-phone"
                                    type="text"
                                    name="phone"
                                    id="phone"
                                    value="<?=$formData['phone'] ?? ""?>"
                                    maxlength="255"
                                    placeholder="Enter Your Phone Number">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12">
                                <label for="message">
                                    Your Message
                                    <small class="text-danger">*</small>
                                </label>
                                <textarea
                                    class="form-control"
                                    id="message"
                                    name="message"
                                    maxlength=65535
                                    placeholder="Sage wisdom for our dearest Guy Smiley"
                                    rows=5
                                    required><?=$formData['message'] ?? ""?></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12">
                                <div
                                    class="g-recaptcha"
                                    data-sitekey="6LcVq0YUAAAAAKMFsTwWDfdnTl6ofHbVlQ3hpbF0"
                                    align="center"
                                    data-callback="enableSubmit"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-12">
                                <button
                                    type="submit"
                                    class="submit btn-lg btn-primary disabled"
                                    >
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </p>
            </div>
        </div>
    </section>

    <!-- Contact Form Scripting -->
    <script src="js/contactForm.js"></script>

<?php include($viewPart['footer']); ?>