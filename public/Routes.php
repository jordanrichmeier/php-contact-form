<?php
namespace DealerInspire;

/**
 * Routes Class
 *
 * @author		Jordan Richmeier
 * @email 		jordan@dealerinspire.com
 *
 * Applies routing to the app object.
 *
 */
class Routes
{
    private $app;
    
    public function __construct($app) {
        $this->app = $app;
        $this->setContactFormRouting();
    }

    public function getRoutedApp() {
        return $this->app;
    }

    private function setContactFormRouting() {
        $this->app->get(
            '/',
            Controller\Contact::class . ':index'
        );
        $this->app->post(
            '/submit',
            Controller\Contact::class . ':handlePost'
        );
    }
}