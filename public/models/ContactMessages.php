<?php
namespace DealerInspire\Model;

/**
 * ContactMessages Class
 *
 * @author  Jordan Richmeier
 * @email   jordan@dealerinspire.com
 *
 * Offers basic CRUD actions to the contacts table
 *
 */
class ContactMessages extends Model
{
    private $tableName = 'contact_messages';
    private $columns = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'message',
        'submitted_at'
    ];

    public function getAll() {
        $statement = $this->db->select()
                              ->from($this->tableName);
        $response = $statement->execute();
        return $response->fetchAll();
    }

    public function get(int $messageId) {
        $statement = $this->db->select()
                              ->from($this->tableName)
                              ->where('id', '=', $messageId);
        $response = $statement->execute();
        return $response->fetch();
    }

    public function getByEmailAddress($email) {
        $statement = $this->db->select()
                              ->from($this->tableName)
                              ->where('email', '=', $email);
        $response = $statement->execute();
        return $response->fetchAll();
    }

    public function getMostRecent() {
        $statement = $this->db->select(array("MAX(id)"))
                              ->from($this->tableName);
        $response = $statement->execute();
        $data = $response->fetch();
        return $this->get($data['MAX(id)']);
    }

    /**
     * @param       $values   Array
     *              Expects:
     *                  first_name
     *                  last_name
     *                  email
     *                  phone
     *                  message
     *
     *  @returns    insert id
     */
    public function insert(array $values) {
        array_push($values, date('Y-m-d H:i:s'));
        $statement = $this->db->insert($this->columns)
                              ->into($this->tableName)
                              ->values($values);
        return $statement->execute();
    }

    /**
     * @param $messageId    RowID
     * @param $values       Array  Must not be empty
     *                      Accepted Array Elements:
     *                          'first_name' => VALUE
     *                          'last_name'  => VALUE
     *                          'email'      => VALUE
     *                          'phone'      => VALUE
     *                          'message'    => VALUE
     */
    public function update(int $messageId, array $values) {
        $this->db->update($values)
                 ->table($this->tableName)
                 ->where('id', '=', $messageId)
                 ->execute();
        return $this;
    }

    public function delete(int $messageId) {
        $this->db->delete()
                 ->from($this->tableName)
                 ->where('id', '=', $messageId)
                 ->execute();
        return $this;
    }

    public function clear() {
        $this->db->delete()
                 ->from($this->tableName)
                 ->execute();
        return $this;
    }
}