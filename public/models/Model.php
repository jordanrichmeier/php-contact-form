<?php
namespace DealerInspire\Model;

/**
 * Model Class
 *
 * @author  Jordan Richmeier
 * @email   jordan@dealerinspire.com
 *
 * Allows basic db access
 *
 */
class Model
{
    protected $db;

    public function __construct($db) {
        $this->db = $db;
    }
}