<?php
namespace DealerInspire\Controller;

use Respect\Validation\Validator as rule;

/**
 * Contact Class
 *
 * @author  Jordan Richmeier
 * @email   jordan@dealerinspire.com
 *
 * Dealer Inspire Code Challenge (ACCEPTED)
 * Contact Form
 *
 */
class Contact extends Controller
{
    public function index($request, $response) {
        return $this->view->render($response, 'challenge.php');
    }

    public function handlePost($request, $response) {
        $data = $request->getParsedBody();
        if ($this->isReCaptchaValid($data) && $this->isPostValid($data)) {
            $messageModel = $this->getModel('ContactMessages');
            $values = [
                $data['fname'],
                $data['lname'],
                $data['email'],
                $data['phone'],
                $data['message']
            ];
            $messageModel->insert($values);

            $mailer = $this->getMailer(
                $this->container->get('settings')['mailer']
            );
            $fullName = $data['fname'] . ' ' . $data['lname'];
            $mailer->setTemplate('contactForm.php', $data)->send(
                'Contact Request',
                [$data['email'] => $fullName],
                'guy-smiley@example.com'
            );
            $this->flash->addMessage(
                'success',
                'Thank you! Guy Smiley will be delighted.'
            );
            return $response->withStatus(202)
                            ->withHeader('Location', '/#contact');
        }

        $this->flash->addMessage('formData', $data);
        return $response->withStatus(302)->withHeader('Location', '/#contact');
    }

    private function isPostValid($postData) {
        $isValid = true;
        $rules = $this->getValidationRules();
        $requiredInputs = [
            'fname' => 'First name',
            'lname' => 'Last name',
            'email' => 'Email address',
            'message' => 'Message'
        ];

        foreach ($requiredInputs as $input => $inputName) {
            if(!$rules->required->validate($postData[$input])) {
                $this->flash->addMessage('danger', "$inputName is required");
                $isValid = false;
            }
        }

        if(!$rules->email->validate($postData['email'])) {
            $this->flash->addMessage(
                'danger',
                'The email address you entered is not in the right format.'
            );
            $isValid = false;
        }

        if($postData['phone'] && !$rules->phone->validate($postData['phone'])) {
            $this->flash->addMessage(
                'danger',
                'Phone numbers should be in the following format: XXX-XXX-XXXX'
            );
            $isValid = false;
        }

        return $isValid;
    }

    private function getValidationRules() {
        $rules = (object) array(
            'required' => rule::notBlank(),
            'email' => rule::email()::filterVar(FILTER_VALIDATE_EMAIL),
            'phone' => rule::phone()
        );
        return $rules;
    }

    private function isReCaptchaValid($postData) {
        if (BY_PASS_CAPTCHA ?? false) {
            return true;
        }

        if (!isset($postData['g-recaptcha-response'])) {
            $this->flash->addMessage('danger', 'ReCaptcha is not set.');
            return false;
        }

        $recaptchaSecret = $this->container->get('settings')['recaptchaSecretKey'];
        $recaptcha = new \ReCaptcha\ReCaptcha(
            $recaptchaSecret,
            new \ReCaptcha\RequestMethod\CurlPost()
        );
        $response = $recaptcha->verify(
            $postData['g-recaptcha-response'],
            $_SERVER['REMOTE_ADDR']
        );

        if (!$response->isSuccess()) {
            $this->flash->addMessage('danger', 'ReCaptcha was not validated.');
            return false;
        }

        return true;
    }
}