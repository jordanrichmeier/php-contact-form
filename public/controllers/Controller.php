<?php
namespace DealerInspire\Controller;

/**
 * Controller Class
 *
 * @author  Jordan Richmeier
 * @email   jordan@dealerinspire.com
 *
 * Base Controller.
 * Handles common tasks for controllers.
 *
 */
class Controller
{
    protected $view;
    protected $container;

    public function __construct(\Slim\Container $container) {
        $this->container = $container;
        $this->flash = new \Slim\Flash\Messages();
        $this->view = new \Slim\Views\PhpRenderer(APP_DIR . '/views');
        $messages = $this->flash->getMessages();
        if ($messages['formData']) {
            $formData = $this->cleanForDisplay($messages['formData'][0]);
            unset($messages['formData']);
        }
        $this->view->setAttributes([
            'viewPart' => [
                'header' => 'header.php',
                'footer' => 'footer.php'
            ],
            'messages' => $messages,
            'formData' => $formData
        ]);
    }

    protected function getMailer() {
        return new \DealerInspire\Library\Mailer(
            $this->container->get('settings')['mailer']
        );
    }

    protected function getModel($name) {
        $dynamicNameSpace = "\\DealerInspire\\Model\\$name";
        return new $dynamicNameSpace($this->container->get('database'));
    }

    private function cleanForDisplay($data) {
        $cleanData = $data;
        foreach ($cleanData as &$value) {
            $value = strip_tags($value);
        }
        return $cleanData;
    }
}