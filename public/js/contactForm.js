$(document).ready(function() {
    forceInputFormatting();
});

function forceInputFormatting() {
    new Cleave('.input-phone', {
        delimiter: '-',
        blocks: [3, 3, 4],
        numericOnly: true
    });
}

function enableSubmit() {
    $('#contactForm button.submit').removeClass('disabled')
                                   .prop('disabled',false);
}